#!/bin/bash


export CI_PROJECT_URL="${CI_PROJECT_URL:-https://gitlab.freedesktop.org/tanty/mesa-mirroring-helper}"
export CI_SERVER_PROTOCOL="${CI_SERVER_PROTOCOL:-https}"
export CI_SERVER_URL="${CI_SERVER_URL:-https://gitlab.freedesktop.org/}"
export FDO_UPSTREAM_REPO="${FDO_UPSTREAM_REPO:-mesa/mesa}"
export UI_GIT_USER_EMAIL=$(git config --get user.email)
export UI_GIT_USER_NAME=$(git config --get user.name)
export UI_MIRRORED_BRANCH="${UI_MIRRORED_BRANCH:-main}"

MMH_LOCAL=1 .gitlab-ci/run-valve-ci-adaptation.sh
