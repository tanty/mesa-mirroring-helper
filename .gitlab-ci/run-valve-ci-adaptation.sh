#!/bin/bash

set -ex

__CURRENT="$PWD"
__COPY=$(realpath "$__CURRENT"/../copy)
__PROJECT=$(echo "${CI_PROJECT_URL}" | sed 's/^'"${CI_SERVER_PROTOCOL}"'\(\|s\):\/\///')
__ORIGIN="${CI_SERVER_PROTOCOL}://oauth2:${UI_OWN_ACCESS_TOKEN}@${__PROJECT}"
__ERROR=0
__PUSH=0
if [ "x$CI_PROJECT_NAMESPACE" == "xtanty" ] && [ "x$CI_COMMIT_REF_NAME" == "xmain" ] && [ "x$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME" != "xmain" ]; then
    __PUSH=1
fi
__LOCAL="${MMH_LOCAL:-0}"
if [ $__LOCAL -eq 1 ]; then
    __PUSH=0
fi

function clean () {
    rm -rf "$__COPY"
}

clean

cd ..
git clone "$__CURRENT" "$__COPY"
cd "$__COPY"
git config user.email "${UI_GIT_USER_EMAIL}"
git config user.name "${UI_GIT_USER_NAME}"
git remote set-url origin "$__ORIGIN"
git fetch origin mesa
git checkout -B mesa origin/mesa
git checkout -B new-mesa origin/mesa
__COMMIT=$(git log --grep "mesa: gitlab-ci files from mesa/${UI_MIRRORED_BRANCH}" --format="%H" -1)

cd mesa
for i in $(find ./ -name "*.yml"); do
    /usr/bin/wget -q -O- ${CI_SERVER_URL}/${FDO_UPSTREAM_REPO}/-/raw/${UI_MIRRORED_BRANCH}/"${i}" > "${i}"
    git add "${i}"
done

git commit -m "mesa: gitlab-ci files from mesa/${UI_MIRRORED_BRANCH}"
git cherry-pick "$__COMMIT"..mesa || __ERROR=1
if ! $(git diff --quiet origin/mesa) && [ $__ERROR -eq 0 ] && [ $__PUSH -eq 1 ]; then
    git push origin new-mesa:mesa
fi

cd "$__CURRENT"

if [ $__LOCAL -ne 1 ] || [ $__ERROR -eq 0 ]; then
    clean
fi

exit $__ERROR
